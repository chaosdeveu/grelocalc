package com.mantz.grelocalc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class exp_res_table extends Activity{
	private ListView table_listview;
	
	final List<HashMap<String, String>> exp_res_table_list = new ArrayList<HashMap<String, String>>();

	String[] from = new String[] {"diff", "exp_1", "exp_2"};
	int[] to = new int[] { R.id.diff, R.id.exp_1, R.id.exp_2};
	SimpleAdapter table_listview_adapter;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exp_res_table);
        
    TextView textview;
    
    textview=(TextView)findViewById(R.id.table_help);
    textview.setText(Html.fromHtml(getResources().getString(R.string.table_help)));
    textview=(TextView)findViewById(R.id.elo_diff);
    textview.setText(Html.fromHtml(getResources().getString(R.string.elo_diff)));
    textview=(TextView)findViewById(R.id.exp_1);
    textview.setText(Html.fromHtml(getResources().getString(R.string.exp_1)));
    textview=(TextView)findViewById(R.id.exp_2);
    textview.setText(Html.fromHtml(getResources().getString(R.string.exp_2)));
    
	double new_exp_result=0, prev_exp_result=-1;
	String start="", diff="";
	
	table_listview = (ListView)findViewById(R.id.listView);

	table_listview_adapter = new SimpleAdapter(this, exp_res_table_list, R.layout.exp_res_table_cell, from, to);
	table_listview.setAdapter(table_listview_adapter);
	
	main dummymain = new main();
	for (int i=0; i<=350; i++){
		new_exp_result=dummymain.calculate_expected_result(i);
		if (new_exp_result != prev_exp_result){
			if (prev_exp_result != -1){
				diff = start + "-" + (i-1);	
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("diff", "" + diff);
				map.put("exp_1", String.format("%.2f", prev_exp_result));
				map.put("exp_2", String.format("%.2f",dummymain.round(1-prev_exp_result, 2)));
				exp_res_table_list.add(map);
			}
			prev_exp_result = new_exp_result;
			start="" + i;
		}
	}
	diff = start + "-350";	
	HashMap<String, String> map = new HashMap<String, String>();
	map.put("diff", "" + diff);
	map.put("exp_1", String.format("%.2f", prev_exp_result));
	map.put("exp_2", String.format("%.2f",dummymain.round(1-prev_exp_result, 2)));
	exp_res_table_list.add(map);
	
	table_listview_adapter.notifyDataSetInvalidated();
	}
}