package com.mantz.grelocalc;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class infoscreen extends Activity{
	
	private TextView textview;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
        
        PackageManager manager = this.getPackageManager();
        PackageInfo info;
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
			textview = (TextView)findViewById(R.id.info_title);
	        textview.setText(Html.fromHtml(this.getString(R.string.infotitle) + " " + info.versionName));
	        textview = (TextView)findViewById(R.id.info_text);
	        textview.setText(Html.fromHtml(this.getString(R.string.infotext)));
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}