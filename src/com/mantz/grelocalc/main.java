package com.mantz.grelocalc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class main extends Activity{

	private EditText edittext_myelo, edittext_oppelo;
	private CheckBox cbox1;
	private Spinner result_spinner;
	private ListView games_listview;
	private TextView textview_change;
	
	private static final int MY = 0, OPP = 1, MIN_ELO = 1000, MAX_ELO = 3000;
	final List<HashMap<String, String>> game_data_list = new ArrayList<HashMap<String, String>>();

	String[] from = new String[] {"opponent_elo", "result", "expected_result", "expected_change"};
	int[] to = new int[] { R.id.opponent_elo, R.id.result, R.id.expected_result, R.id.expected_change };
	SimpleAdapter games_listview_adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		edittext_myelo = (EditText)findViewById(R.id.my_elo);
		edittext_oppelo = (EditText)findViewById(R.id.opponent_elo);
		cbox1 = (CheckBox) findViewById(R.id.checkBox_k10);
		result_spinner = (Spinner) findViewById(R.id.result);
		games_listview = (ListView)findViewById(R.id.listView1);
		textview_change = (TextView)findViewById(R.id.expected_change);

		games_listview_adapter = new SimpleAdapter(this, game_data_list, R.layout.gamerow, from, to);
		games_listview.setAdapter(games_listview_adapter);

		/* The listener for the K=10 flag */
		cbox1.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				recalculate();
			}
		});

		/* populate the results spinner */
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.gameresult, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		result_spinner.setAdapter(adapter);

		/* The listener for the games list */
		games_listview.setOnItemClickListener(new ListView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> a, View v, final int position, long id) {
				alert_delete_game(position);
			}
		});

		/* Listener for the insert game button */
		Button b = (Button) findViewById(R.id.btnClick3);
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				insert_game();
			}
		});    

		/* Listener for my_elo text field */
		edittext_myelo.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				recalculate();
			}
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
		});

		restore_data();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent myIntent = new Intent();
		
		switch (item.getItemId()) {
		case R.id.iconclear:
			alert_clear_games();
			break;
		case R.id.iconinfo:
	    	myIntent.setClassName("com.mantz.grelocalc", "com.mantz.grelocalc.infoscreen");
	    	startActivity(myIntent); 
			break;
		case R.id.iconhelp:
	    	myIntent.setClassName("com.mantz.grelocalc", "com.mantz.grelocalc.helpscreen");
	    	startActivity(myIntent); 
			break;
		case R.id.iconexit:
			finish();
			break;
		}
		return true;
	}	

	protected void onPause() {
		super.onPause();

		/* Store data */
		Editor e = this.getPreferences(Context.MODE_PRIVATE).edit();
		if (edittext_myelo.length() != 0) e.putString("stored_my_elo", edittext_myelo.getText().toString());
		else e.putString("stored_my_elo", "1000");
		if (cbox1.isChecked()) e.putString("cbox1", "Checked");
		else e.putString("cbox1", "Unchecked");
		e.commit();
		store_adapter();
	}

	void clear_game_list(){
		int no_games;

		no_games = games_listview_adapter.getCount();
		for (int i = no_games -1; i >= 0 ; i--)
		{
			game_data_list.remove(i);
		}
		games_listview_adapter.notifyDataSetInvalidated();
		recalculate();
		Toast.makeText(this, "Τα παιχνίδια διαγράφηκαν", Toast.LENGTH_SHORT).show();
		return;
	}

	/*
	 * restore_data()
	 * Call on startup to restore data saved from previous session
	 */
	void restore_data(){
		String tmp_str;

		tmp_str = getPreferences(MODE_PRIVATE ).getString( "stored_my_elo" , "1500" );
		edittext_myelo.setText(tmp_str);

		tmp_str = getPreferences(MODE_PRIVATE ).getString( "cbox1" , "Unchecked" );
		if (tmp_str.equals("Checked")) cbox1.setChecked(true);
		else cbox1.setChecked(false);

		restore_adapter();
	}
	
	/*
	 * insert_game()
	 * Insert a game in the games list
	 */

	void insert_game(){
		int my_elo,diff, opponent_elo;
		double result = 0, expected_result, expected_change;
		String tmp_str;
		
		if ( edittext_myelo.length() == 0){
			alert_elo_missing(MY);
			return;
		}
		if ( edittext_oppelo.length() == 0){
			alert_elo_missing(OPP);
			return;
		}
		
		opponent_elo = Integer.parseInt(edittext_oppelo.getText().toString());
		my_elo = Integer.parseInt(edittext_myelo.getText().toString());
		
		if ((opponent_elo < MIN_ELO) || (opponent_elo > MAX_ELO)){
			alert_elo_out_of_bounds();
			return;
		}
		if ((my_elo < MIN_ELO) || (my_elo > MAX_ELO)){
			alert_elo_out_of_bounds();
			return;
		}
		
		diff = calculate_diff(my_elo,opponent_elo);
		expected_result = calculate_expected_result(diff);
		
		tmp_str= result_spinner.getSelectedItem().toString();

		if (tmp_str.equals("+")) result = 1;
		if (tmp_str.equals("=")) result = 0.5;

		expected_change = calculate_expected_change(my_elo, diff, result, 1);

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("opponent_elo", "" + opponent_elo);
		map.put("result", "  " + tmp_str + "  ");
		map.put("expected_result", "" + expected_result);
		map.put("expected_change", "" + expected_change);
		game_data_list.add(map);

		games_listview_adapter.notifyDataSetInvalidated();

		recalculate();
	}
	
	/*
	 * store_adapter()
	 * Will convert the games list to a string and store it.
	 */

	void store_adapter(){
		String encoded_adapter="", tmp_str="", result="", opp_elo="";
		int no_games=0;

		no_games = games_listview_adapter.getCount();
		for (int i = 0; i < no_games ; i++)
		{
			HashMap<String, String> game_data_map = (HashMap<String, String>) games_listview_adapter.getItem(i);
			Iterator myVeryOwnIterator = game_data_map.keySet().iterator();
			while(myVeryOwnIterator.hasNext()) {
				String key=(String)myVeryOwnIterator.next();
				tmp_str=(String)game_data_map.get(key);
				if ( key == "result"){
					if (tmp_str.equals("  +  ")) result="+";
					if (tmp_str.equals("  =  ")) result="=";
					if (tmp_str.equals("  -  ")) result="-";
				}
				if ( key == "opponent_elo") opp_elo = tmp_str;
			} 
			encoded_adapter = encoded_adapter + result + opp_elo;
		}

		Editor e = this.getPreferences(Context.MODE_PRIVATE).edit();
		e.putString("stored_games_adapter", encoded_adapter);
		e.commit();

		return ;
	}

	/*
	 * restore_adapter()
	 * Will restore the games list string and restore it
	 * in the game_data_list
	 */

	void restore_adapter(){
		String encoded_adapter="", result="", opp_elo="";
		int no_games=0, tmp_int;

		encoded_adapter = getPreferences(MODE_PRIVATE ).getString( "stored_games_adapter" , "" );
		no_games = encoded_adapter.length() / 5;
		if ( (encoded_adapter.length() % 5) != 0) return;

		for (int i = 0; i < no_games ; i++)
		{
			result = encoded_adapter.substring(5 * i, 5 * i + 1);
			opp_elo = encoded_adapter.substring(5 * i + 1, 5 * ( i + 1));
			if ( !result.equals("+") && !result.equals("-")  && !result.equals("=") ) continue;
			try{
				tmp_int = Integer.parseInt(opp_elo);
				if ( (tmp_int < MIN_ELO) || (tmp_int > MAX_ELO) ) continue;
			}
			catch(NumberFormatException nfe){
				continue;
			}

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("opponent_elo", "" + opp_elo);
			map.put("result", "  " + result + "  ");
			map.put("expected_result", "");
			map.put("expected_change", "");
			game_data_list.add(map);
		}
		recalculate();
		return ;

	}
	
	/*
	 * alert_clear_games()
	 * Will raise an Alert Dialog to confirm the deletion
	 * of all entered games
	 */
	void alert_clear_games(){
		int no_games = games_listview_adapter.getCount();
		if ( no_games == 0) return;
		AlertDialog.Builder adb=new AlertDialog.Builder(main.this);
		adb.setTitle("Εκκαθάριση;");
		adb.setMessage("Θέλετε να διαγράψετε όλα τα παιχνίδια;");
		adb.setNegativeButton  ("Άκυρο", null);
		adb.setPositiveButton  ("Εκκαθάριση", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				clear_game_list();
			}
		});
		adb.show();
	}

	/*
	 * alert_delete_game()
	 * int position: The position in the list of the game to be deleted
	 * will raise an Alert Dialog to confirm the deletion of a game
	 * from the games list
	 */

	void alert_delete_game(final int position){
		String tmp_str;
		
		AlertDialog.Builder adb=new AlertDialog.Builder(main.this);
		HashMap<String, String> game_data_map = (HashMap<String, String>) games_listview_adapter.getItem(position);
		Iterator myVeryOwnIterator = game_data_map.keySet().iterator();
		String opp_str="", res_str="";
		while(myVeryOwnIterator.hasNext()) {
			String key=(String)myVeryOwnIterator.next();
			tmp_str=(String)game_data_map.get(key);
			if ( key == "result") res_str = tmp_str;
			if ( key == "opponent_elo") opp_str = tmp_str;
		} 
		adb.setTitle("Διαγραφή παιχνιδιού;");
		adb.setMessage("elo αντιπάλου :" + opp_str + "\nαποτέλεσμα : " + res_str);
		adb.setPositiveButton  ("Διαγραφή", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				game_data_list.remove(position);
				games_listview_adapter.notifyDataSetInvalidated();
				recalculate(); 
			}
		});
		adb.setNegativeButton  ("Άκυρο", null);
		adb.show();
	}

	/*
	 * alert_elo_missing(int whose)
	 * whose: MY, OPP
	 * will raise an Alert Dialog to notify the my or opponent's
	 * elo has not been entered.
	 */

	void alert_elo_missing(int whose){
		AlertDialog.Builder adb=new AlertDialog.Builder(main.this);
		if ( whose == MY){
			adb.setTitle("Συμπληρώστε το elo");
			adb.setMessage("Παρακαλώ συμπληρώστε το elo σας");
		} else if ( whose == OPP) {
			adb.setTitle("Συμπληρώστε το elo");
			adb.setMessage("Παρακαλώ συμπληρώστε το elo του αντιπάλου σας");
		} else {
			return;
		}
		adb.setPositiveButton  ("Συνέχεια", null);
		adb.show();
		return;	
	}

	/*
	 * alert_elo_out_of_bounds(int whose)
	 * whose: MY, OPP
	 * will raise an Alert Dialog to notify the my or opponent's
	 * elo is out of bounds.
	 */

	void alert_elo_out_of_bounds(){
		AlertDialog.Builder adb=new AlertDialog.Builder(main.this);
		adb.setTitle("Άκυρο elo");
		adb.setMessage("Το elo πρέπει να είναι από " + MIN_ELO + " έως " + MAX_ELO);
		adb.setPositiveButton  ("Συνέχεια", null);
		adb.show();
		return;	
	}

	/*
	 * double calculate_expected_change(int my_elo, int diff, double result, int games)
	 * int my_elo   : My current elo rating
	 * int diff     : Difference from average opponents' elo, 
	 *                positive number means my elo is higher than opponents
	 * double result: Total points won
	 * int games    : Total games played
	 * 
	 * return       : Expected elo change according to input data.
	 */
	private double calculate_expected_change(int my_elo, int diff, double result, int games){
		int base = 0;
		double change, exp_result;


		if (cbox1.isChecked()) base = 10;
		else if (my_elo <= 1400) base = 35;
		else if (my_elo <= 1800) base = 25;
		else if (my_elo <= 2400) base = 15;
		else if (my_elo >  2400) base = 10;

		exp_result = calculate_expected_result(diff) * games;

		change = base * (result - exp_result);

		return(round(change, 2));
	}

	/*
	 * double calculate_expected_result(int diff)
	 * 
	 * int diff: Difference from average opponents' elo, 
	 *           positive number means my elo is higher than opponents
	 *           
	 * return  : The expected game result, according to the input data
	 * 
	 */

	public double calculate_expected_result(int diff){

		double exp_result=0;
		int flag=0;

		if (diff < 0){
			diff = -diff;
			flag=1;
		}

		if      (diff > 344) exp_result = 0.89;
		else if (diff > 328) exp_result = 0.88;
		else if (diff > 315) exp_result = 0.87;
		else if (diff > 302) exp_result = 0.86;
		else if (diff > 290) exp_result = 0.85;
		else if (diff > 278) exp_result = 0.84;
		else if (diff > 267) exp_result = 0.83;
		else if (diff > 256) exp_result = 0.82;
		else if (diff > 245) exp_result = 0.81;
		else if (diff > 235) exp_result = 0.80;
		else if (diff > 225) exp_result = 0.79;
		else if (diff > 215) exp_result = 0.78;
		else if (diff > 206) exp_result = 0.77;
		else if (diff > 197) exp_result = 0.76;
		else if (diff > 188) exp_result = 0.75;
		else if (diff > 179) exp_result = 0.74;
		else if (diff > 170) exp_result = 0.73;
		else if (diff > 162) exp_result = 0.72;
		else if (diff > 153) exp_result = 0.71;
		else if (diff > 145) exp_result = 0.70;
		else if (diff > 137) exp_result = 0.69;
		else if (diff > 129) exp_result = 0.68;
		else if (diff > 121) exp_result = 0.67;
		else if (diff > 113) exp_result = 0.66;
		else if (diff > 106) exp_result = 0.65;
		else if (diff > 98 ) exp_result = 0.64;
		else if (diff > 91 ) exp_result = 0.63;
		else if (diff > 83 ) exp_result = 0.62;
		else if (diff > 76 ) exp_result = 0.61;
		else if (diff > 68 ) exp_result = 0.60;
		else if (diff > 61 ) exp_result = 0.59;
		else if (diff > 53 ) exp_result = 0.58;
		else if (diff > 46 ) exp_result = 0.57;
		else if (diff > 39 ) exp_result = 0.56;
		else if (diff > 32 ) exp_result = 0.55;
		else if (diff > 25 ) exp_result = 0.54;
		else if (diff > 17 ) exp_result = 0.53;
		else if (diff > 10 ) exp_result = 0.52;
		else if (diff > 3  ) exp_result = 0.51;
		else if (diff >=0  ) exp_result = 0.50;

		if (flag ==  1) exp_result = 1 - exp_result;

		return(round(exp_result, 2));

	}

	/*
	 * double round(double original, int precision )
	 * double original: The double variable we want to round
	 * int precision  : The number of decimal digits we want
	 * 
	 * return         : The rounded number
	 */

	public double round(double original, int precision ){
		BigDecimal original_bd = new BigDecimal(original);
		BigDecimal original_rounded = original_bd.setScale(precision, BigDecimal.ROUND_HALF_UP );
		return original_rounded.doubleValue();
	}

	/*
	 * int calculate_diff(int my_elo, int opponent_elo)
	 * int my_elo      : My current elo rating
	 * int opponent_elo: Opponent's current elo rating
	 * 
	 * return          : my_elo - opponent_elo
	 * 
	 * The difference can't be higher/lower than 350
	 * If more than 350 is calculated, 350 is returned instead
	 */

	private int calculate_diff(int my_elo, int opponent_elo){
		int diff;

		diff=my_elo-opponent_elo;
		if (diff > 350) diff=350;
		if (diff < -350) diff=-350;
		return(diff);
	}

	/*
	 * void recalculate()
	 * Go through the games list, and recalculate all expected result ann expected change
	 * In addition, recalculate the total expected change
	 */

	private void recalculate(){
		int total = 0,diff=0,opp_elo=0, my_elo=0, no_games;
		double act_result=0, change, exp_result;
		String tmp_str;

		no_games = games_listview_adapter.getCount();
		if (no_games != 0){
			if ( edittext_myelo.length() == 0 ) return;
			my_elo = Integer.parseInt(edittext_myelo.getText().toString());
			if ( (my_elo < MIN_ELO) || (my_elo > MAX_ELO) ) return;

			for (int i = 0; i < no_games ; i++)
			{
				double game_result = 0;
				HashMap<String, String> meMap = (HashMap<String, String>) games_listview_adapter.getItem(i);
				Iterator myVeryOwnIterator = meMap.keySet().iterator();
				while(myVeryOwnIterator.hasNext()) {
					String key=(String)myVeryOwnIterator.next();
					tmp_str=(String)meMap.get(key);
					if ( key == "result"){
						if (tmp_str.equals("  +  ")) {
							act_result=act_result+1;
							game_result = 1;
						}
						if (tmp_str.equals("  =  ")) {
							act_result=act_result+0.5;
							game_result = 0.5;
						}
					}
					if ( key == "opponent_elo") opp_elo = Integer.parseInt(tmp_str);
				} 

				diff=calculate_diff(my_elo,opp_elo);
				total = total + diff;

				exp_result = calculate_expected_result(diff);
				change = calculate_expected_change(my_elo, diff, game_result, 1);

				meMap.put("expected_result", "" + exp_result);
				meMap.put("expected_change", "" + change);
			}

			games_listview_adapter.notifyDataSetInvalidated();

			total = total / no_games;

			exp_result=calculate_expected_result(total);
			change = calculate_expected_change(my_elo, total, act_result, no_games);
			
			TextView textview = (TextView)findViewById(R.id.tot_result);
				textview.setText(Double.toString(act_result));
			textview = (TextView)findViewById(R.id.tot_opponent_elo);
				textview.setText(Integer.toString(my_elo-total));
			textview = (TextView)findViewById(R.id.tot_expected_result);
				textview.setText(Double.toString(round(exp_result*no_games,2)));
			textview = (TextView)findViewById(R.id.tot_expected_change);
				textview.setText(Double.toString(change));
			textview = (TextView)findViewById(R.id.no_games);
				textview.setText(Integer.toString(no_games));
			
			

			if (change < 0) textview_change.setTextColor(Color.RED);
			else textview_change.setTextColor(Color.GREEN);
			textview_change.setText(Double.toString(change));
		} else {
			textview_change.setTextColor(Color.GREEN);
			textview_change.setText("0");
		}
	}   

}