package com.mantz.grelocalc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class helpscreen extends Activity{
	
	private TextView textview;
	
	final List<HashMap<String, String>> exp_res_table_list = new ArrayList<HashMap<String, String>>();

	String[] from = new String[] {"diff", "exp_1"};
	int[] to = new int[] { R.id.diff, R.id.exp_1, };
	SimpleAdapter table_listview_adapter;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);
	
        textview=(TextView)findViewById(R.id.textView1);
        textview.setText(Html.fromHtml(getResources().getString(R.string.helptext)));
        
		Button b = (Button) findViewById(R.id.btnClick1);
		b.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent myIntent = new Intent();
				myIntent.setClassName("com.mantz.grelocalc", "com.mantz.grelocalc.exp_res_table");
		    	startActivity(myIntent); 
			}
		});   
	}	
}